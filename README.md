## List of papers from [CSCI E-191](https://canvas.harvard.edu/courses/69556/assignments/syllabus): Classics of Computer Science.

[Leviathan](https://books.google.com/books?id=2oc6AAAAMAAJ)

[Preface to the general science](https://books.google.com/books?id=dLQ3bDy2tgYC&pg=PA189&lpg=PA189&dq=leibniz+%22preface+to+the+general+science%22&source=bl&ots=OAmP9tWGzI&sig=uCdYIXRRLE2A9U7cu1X3CotJHr4&hl=en&sa=X&ved=0ahUKEwiTgsumkf_QAhVFSiYKHSTtA3MQ6AEIGjAA#v=onepage&q=leibniz%20%22preface%20to%20the%20general%20science%22&f=false)

[The Monadology](https://books.google.com/books?id=s0PXAAAAMAAJ)

[A sketch of the analytical engine invented by Charles Babbage](https://www.fourmilab.ch/babbage/sketch.html)

[An investigation of the laws of thought](http://www.eng.auburn.edu/~agrawvd/COURSE/READING/DIGITAL/15114-pdf.pdf)

[Logical Machines, Am J. Psych. 1, p. 165](https://books.google.com/books?id=b8MwWI1ClGIC&pg=PR35)

[Mathematical problems](http://aleph0.clarku.edu/~djoyce/hilbert/problems.html)

[When perforated paper goes to work, Sci. Am., 127, p. 395](https://zenodo.org/record/2148877/files/article.pdf?download=1)

[On computable numbers, with an application to the Entscheidungsproblem](https://www.cs.virginia.edu/~robins/Turing_Paper_1936.pdf)

[Proposed automatic calculating machine](http://history-computer.com/Library/AikenProposal.pdf)

[A symbolic analysis of relays and switching circuits](https://paradise.caltech.edu/ist4/lectures/shannon38.pdf)

[A logical calculus of the ideas immanent in nervous activity](http://www.cse.chalmers.se/~coquand/AUTOMATA/mcp.pdf)

[As we may think](http://www.theatlantic.com/magazine/archive/1945/07/as-we-may-think/303881/)

[First draft of a report on the EDVAC](http://www.science.smith.edu/dftwiki/images/f/f8/VonNewmannEdvac.pdf)

[Preliminary discussion of the logical design of an electronic computing instrument](https://www.cs.princeton.edu/courses/archive/fall10/cos375/Burks.pdf)

[A mathematical theory of communication](http://worrydream.com/refs/Shannon%20-%20A%20Mathematical%20Theory%20of%20Communication.pdf)

[The general and logical theory of automata](https://www.cs.ucf.edu/~dcm/Teaching/COP5611Spring2010/vonNeumannSelfReproducingAutomata.pdf)

[The brain as a computing machine](https://cepa.info/?download=2828)

[Error detecting and error correcting codes](https://signallake.com/innovation/hamming.pdf)

[Programming a computer for playing chess](http://vision.unipv.it/IA1/ProgrammingaComputerforPlayingChess.pdf)

[Computing Machinery and Intelligence](http://www.loebner.net/Prizef/TuringArticle.html)

[Representation of events in nerve nets and finite automata](https://www.rand.org/content/dam/rand/pubs/research_memoranda/2008/RM704.pdf)

[The best way to design an automatic calculating machine](https://www.cs.princeton.edu/courses/archive/fall10/cos375/BestWay.pdf)

[The education of a computer](https://doi.org/10.1109/MAHC.1987.10032)

[Computers and automata](https://doi.org/10.1109/JRPROC.1953.274273)

[A proposal for the Dartmouth summer institute on artifical intelligence](http://www.aaai.org/ojs/index.php/aimagazine/article/viewFile/1904/1802)

[Semiconductor research leading to the point contact transistor](https://www.nobelprize.org/uploads/2018/06/bardeen-lecture.pdf)

[Letter to von Neumann](http://www.cs.cmu.edu/~15251/notes/godel-letter.pdf)

[The logic theory machine](https://doi.org/10.1109/TIT.1956.1056797)

[Three Models for the Description of Language](http://static.stevereads.com/papers_to_read/three_models_for_the_description_of_language.pdf)

[The FORTRAN automatic coding system](https://archive.computerhistory.org/resources/text/Fortran/102663113.05.01.acc.pdf)

[The perceptron: A probabilistic model for information storage and organization in the brain](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.335.3398&rep=rep1&type=pdf)

[Finite automata and their decision problems](http://www.cse.chalmers.se/~coquand/AUTOMATA/rs.pdf)

[Man-Computer Symbiosis](http://worrydream.com/refs/Licklider%20-%20Man-Computer%20Symbiosis.pdf)

[Reliable digital communications systems utilizing unreliable network repeater nodes](https://www.rand.org/content/dam/rand/pubs/papers/2008/P1995.pdf)

[Recursive functions of symbolic expressions and their computation by machine, Part I](https://aiplaybook.a16z.com/reference-material/mccarthy-1960.pdf)

[An experimental time-sharing system](http://larch-www.lcs.mit.edu:8001/~corbato/sjcc62/)

[Augmenting human intellect](http://www.1962paper.org/web.html)

[College admissions and the stability of marriage](http://cramton.umd.edu/market-design/gale-shapley-college-admissions.pdf)

[Quicksort](https://comjnl.oxfordjournals.org/content/5/1/10.full.pdf)

[One-level storage system](http://www.chilton-computing.org.uk/acl/pdfs/atlas-1-level.pdf)

[A theorem on boolean matrices](http://bioinfo.ict.ac.cn/~dbu/AlgorithmCourses/Lectures/Warshall1962.pdf)

[On the computational complexity of algorithms](http://fi.ort.edu.uy/innovaportal/file/20124/1/60-hartmanis_stearns_complexity_of_algorithms.pdf)

[Revised report on the algorithmic language ALGOL 60](http://web.eecs.umich.edu/~bchandra/courses/papers/Naure_Algol60.pdf)

[Sketchpad, a man-machine communication system](https://www.cl.cam.ac.uk/techreports/UCAM-CL-TR-574.pdf)

[Architecture of the IBM System/360](https://doi.org/10.1147/rd.82.0087)

[On distributed communications](http://www.rand.org/content/dam/rand/pubs/research_memoranda/2006/RM3420.pdf)

[Miniaturized electronic circuits](https://www.google.com/patents/US3138743)

[Paths, trees, and flowers](http://math.nist.gov/~JBernal/p_t_f.pdf)

[An algorithm for the machine calculation of complex Fourier series](https://www.ams.org/journals/mcom/1965-19-090/S0025-5718-1965-0178586-1/S0025-5718-1965-0178586-1.pdf)

[On the translation of languages from left to right](https://www.sciencedirect.com/science/article/pii/S0019995865904262)

[Cramming more components onto integrated circuits](https://newsroom.intel.com/wp-content/uploads/sites/11/201/05/moores-law-electronics.pdf)

[The ultimate display](http://worrydream.com/refs/Sutherland%20-%20The%20Ultimate%20Display.pdf)

[Slave memories and dynamic storage allocation](https://www.cs.princeton.edu/courses/archive/fall10/cos375/WilkesCacheElectronics.pdf)

[How do you solve a quadratic equation?](http://i.stanford.edu/pub/cstr/reports/cs/tr/66/40/CS-TR-66-40.pdf)

[ELIZA -- A computer program for the study of natural langauage communication between man and machine](http://web.stanford.edu/class/linguist238/p36-weizenabaum.pdf)

[A machine-independent theory of the complexity of recursive functions](https://doi.org/10.1145/321386.321395)

[The structure of the "THE" multiprogramming system](https://klevas.mif.vu.lt/~liutauras/books/Dijkstra%20-%20The%20structure%20of%20the%20THE%20multiprogramming%20system.pdf)

[Assigning meanings to programs](http://www.cs.tau.ac.il/~nachumd/term/FloydMeaning.pdf)

[An efficient algorithm for exploiting multiple arithmetic units](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.129.4915&rep=rep1&type=pdf)

[Virtual memory, processes, and sharing in MULTICS](http://www.cs.utexas.edu/users/dahlin/Classes/GradOS/papers/p306-daley.pdf)

[The working set model for program behavior](http://www.liralab.it/teaching/OS/files/p323-denning.pdf)

[Go To Statement considered harmful](http://homepages.cwi.nl/~storm/teaching/reader/Dijkstra68.pdf)

[FJCC "The mother of all demos"](http://web.stanford.edu/dept/SUL/library/extra4/sloan/mousesite/1968Demo)

[On the design of display processors](http://cva.stanford.edu/classes/cs99s/papers/myer-sutherland-design-of-display-processors.pdf)

[An axiomatic basis for computer programming](https://www.cs.cmu.edu/~crary/819-f09/Hoare69.pdf)

[Gaussian Elimination is not Optimal](https://doi.org/10.1007/BF02165411)

[A reltional model for large shared data banks](https://www.seas.upenn.edu/~zives/03f/cis550/codd.pdf)

[An efficient context-free parsing algorithm](http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.138.1808&rank=1)

[Intellectual implications of multi-access computer networks](http://www.dougengelbart.org/pubs/augment-5255.html)

[The computer as a communication device](http://memex.org/licklider.pdf)

[The home information terminal](http://www-formal.stanford.edu/jmc/hoter2.pdf)

[Outline of a mathematical theory of computation](https://www.cs.ox.ac.uk/files/3222/PRG02.pdf)

[The complexity of theorem-proving procedures](https://www.cs.toronto.edu/~sacook/homepage/1971.pdf)

[Toward a mathematical semantics of programming languages](https://www.cs.ox.ac.uk/files/3228/PRG06.pdf)

[Organization and maintenance of large ordered indexes](http://mathcs.pugetsound.edu/~dchiu/teaching/archive/CS455sp15/papers/btree.pdf)

[SPACEWAR: Fanatic life and symbolic death among the computer bums](http://www.wheels.org/spacewar/stone/rolling_stone.html)

[Relational completeness of data base sublanguages](http://www.geology.cz/personal/j/jan.sedlacek/codd2.pdf)

[Reducibility among combinatorial problems](http://cgi.di.uoa.gr/~sgk/teaching/grad/handouts/karp.pdf)

[A personal computer for children of all ages](http://mprove.de/diplom/gui/kay72.html)

[A statistical interpretation of term specificity and its application in retrieval](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.115.8343&rep=rep1&type=pdf)

[Time bounds for selection](https://people.csail.mit.edu/rivest/pubs/BFPRT73.pdf)

[Universal search problems](https://rjlipton.wordpress.com/2011/03/14/levins-great-discoveries/)

[A subdivision algorithm for display of curved surfaces](http://static1.1.sqspcdn.com/static/f/552576/6419248/1270507173137/catmull_thesis.pdf?token=HytCG9faeSevSXpD01dUP2bsvoU%3D)

[A protocol for packet network intercommunication](https://apps.dtic.mil/dtic/tr/fulltext/u2/a634240.pdf)

[Programming with abstract data types](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.136.3043&rep=rep1&type=pdf)

[The UNIX time-sharing system](https://people.eecs.berkeley.edu/~brewer/cs262/unix.pdf)

[The Mythical Man-Month](https://archive.org/details/mythicalmanmonth00fred)

[Strawman requirements](http://www.iment.com/maida/computer/requirements/strawman.htm)

[Granularity of locks in a shared data base](http://pages.cs.wisc.edu/~nil/764/Trans/13_P428.pdf)

[A vector space model for automatic indexing](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.446.5101&rep=rep1&type=pdf)

[A program data flow analysis procedure](https://amturing.acm.org/p137-allen.pdf)

[A lattice model of secure information flow](https://pdfs.semanticscholar.org/5f2b/22b77559ddb4f3734459d1ff66c58d22df12.pdf)

[New Directions in Cryptography](https://www-ee.stanford.edu/~hellman/publications/24.pdf)

[Ethernet: Distributed Packet switching for local computer networks](http://www.cs.cmu.edu/~srini/15-744/papers/MB76.pdf)

[Personal dynamic media](http://www.newmediareader.com/book_samples/nmr-26-kay.pdf)

[Abstraction mechanisms in CLU](https://web.eecs.umich.edu/~weimerw/2008-615/reading/liskov-clu-abstraction.pdf)

[A theory of type polymorphism in programming](http://web.cs.wpi.edu/~cs4536/c12/milner-type-poly.pdf)

[The progression of realism in computer generated images](https://pdfs.semanticscholar.org/a1fb/09ff51242a245572c6247d17ee93afa7e3da.pdf)

[Awk — A Pattern Scanning and Processing Language](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.31.1299&rep=rep1&type=pdf)

[Time, clocks, and the ordering of events in a distributed system](http://amturing.acm.org/p558-lamport.pdf)

[Communicating sequential processes](https://www.cs.cmu.edu/~crary/819-f09/Hoare78.pdf)

[A method for obtaining digital signatures and public-key cryptosystems](http://people.csail.mit.edu/rivest/Rsapaper.pdf)

[The Cray-1 computer system](https://www.cs.auckland.ac.nz/courses/compsci703s1c/archive/2008/resources/Russell.pdf)

[Universal classes of hash functions](https://www.cs.princeton.edu/courses/archive/fall09/cos521/Handouts/universalclasses.pdf)

[Social processes and proofs of theorems and programs](https://www.cs.umd.edu/~gasarch/BLOGPAPERS/social.pdf)

[Alto: A personal computer](http://research.microsoft.com/en-us/um/people/blampson/25-Alto/25-Alto.pdf)

[Minds, Brains, and Programs](http://cogprints.org/7150/1/10.1.1.83.5248.pdf)

[An improved illumination model for shaded display](https://pdfs.semanticscholar.org/b1d7/6a254801a09916479659160fd839c905ae87.pdf)

[Psychologism and Behaviorism](http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.4.5828)

[End to end arguments in system design](http://fab.cba.mit.edu/classes/S62.12/docs/Saltzer_system.pdf)

[The emperor's old clothes](http://zoo.cs.yale.edu/classes/cs422/2014/bib/hoare81emperor.pdf)

[Principal type-schemes for functional languages](http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.645.5233)

[Literate programming](http://www.literateprogramming.com/knuthweb.pdf)

[Formalization in program development](https://link.springer.com/article/10.1007/BF01934408)

[Technology and courage](http://vlsicad.ucsd.edu/Research/Advice/technologyAndCourage.pdf)

[Using cache memory to reduce processor-memory traffic](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.436.5134&rep=rep1&type=pdf)

[Hints for computer system design](http://research.microsoft.com/en-us/um/people/blampson/33-Hints/WebPage.html)

[Probabilistic encryption](https://groups.csail.mit.edu/cis/pubs/shafi/1984-jcss.pdf)

[Reflections on trusting trust](https://www.ece.cmu.edu/~ganger/712.fall02/papers/p761-thompson.pdf)

[A theory of the learnable](https://people.mpi-inf.mpg.de/~mehlhorn/SeminarEvolvability/ValiantLearnable.pdf)

[The knowledge complexity of interactive proof systems](https://groups.csail.mit.edu/cis/pubs/shafi/1985-stoc.pdf)

[Evidence against the context-freeness of natural languages](https://www.eecs.harvard.edu/shieber/Biblio/Papers/shieber85.pdf)

[The rendering equation](http://www.dca.fee.unicamp.br/~leopini/DISCIPLINAS/IA725/ia725-12010/kajiya-SIG86-p143.pdf)

[Learning representations by back-propagating errors](https://web.stanford.edu/class/psych209a/ReadingsByDate/02_06/PDPVolIChapter8.pdf)

[No silver bullet](http://worrydream.com/refs/Brooks-NoSilverBullet.pdf)

[Data abstraction and hierarchy](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.12.819&rep=rep1&type=pdf)

[Marching cubes: A high resolution 3D surface construction algorithm](http://academy.cba.mit.edu/classes/scanning_printing/MarchingCubes.pdf)

[The computer scientist as toolsmith](http://cs.unc.edu/xcms/wpfiles/toolsmith/The_Computer_Scientist_as_Toolsmith.pdf)

[The design philosophy of the DARPA Internet protocols](http://www.cs.princeton.edu/~jrex/teaching/spring2005/reading/clark88.pdf)

[Congestion avoidance and control](http://ee.lbl.gov/papers/congavoid.pdf)

[A case for redundant arrays of inexpensive disks (RAID)](https://www.cs.cmu.edu/~garth/RAIDpaper/Patterson88.pdf)

[Information management: a proposal](https://faculty.georgetown.edu/irvinem/theory/Berners-Lee-HTTP-proposal.pdf)

[An introduction to programming with threads](https://birrell.org/andrew/papers/035-Threads.pdf)

[A logic of authentication](http://www.hpl.hp.com/techreports/Compaq-DEC/SRC-RR-39.pdf)

[The rise of "Worse is better"](https://www.jwz.org/doc/worse-is-better.html)

[World-wide web: The information universe](http://www.emeraldgrouppublishing.com/products/backfiles/pdf/backfiles_sample_5.pdf)

[Authentication in distributed systems](http://research.microsoft.com/en-us/um/people/blampson/45-AuthenticationTheoryAndPractice/WebPage.html)

[Why cryptosystems fail](http://www.cl.cam.ac.uk/~rja14/Papers/wcf.pdf)

[Random early detection gateways for congestion avoidance](http://www.icir.org/floyd/papers/early.twocolumn.pdf)

[An investigation of the Therac-25 accidents](https://web.stanford.edu/class/cs240/old/sp2014/readings/therac-25.pdf)

[TCP and explicit congestion notification](https://cseweb.ucsd.edu/classes/wi01/cse222/papers/floyd-ecn-ccr94.pdf)

[A behavioral notion of subtyping ](http://csnell.net/computerscience/Liskov_subtypes.pdf)

[The computer scientist as toolsmith II](http://www.cs.unc.edu/~brooks/Toolsmith-CACM.pdf)

[The case for a single-chip multiprocessor](https://www.cs.cmu.edu/afs/cs/academic/class/15740-s17/www/papers/onhwc96.pdf)

[The anatomy of a large-scale hypertextual web search engine](http://infolab.stanford.edu/~backrub/google.html)

[The PageRank citation ranking: Bringing order to the Web](http://ilpubs.stanford.edu:8090/422/1/1999-66.pdf)

[Growing a language](https://www.cs.virginia.edu/~evans/cs655/readings/steele.pdf)

[Next century challenges: Scalable coordination in sensor networks](http://www.isi.edu/~johnh/PAPERS/Estrin99e.pdf)

[Freenet](http://lsirwww.epfl.ch/courses/dis/2003ws/papers/clarke00freenet.pdf)

[MapReduce: Simplifed data processing on large clusters](https://static.googleusercontent.com/media/research.google.com/en//archive/mapreduce-osdi04.pdf)

[Differential privacy](http://link.springer.com.ezp-prod1.hul.harvard.edu/chapter/10.1007/11787006_1)

[The man who tried to redeem the world with logic](http://nautil.us/issue/21/information/the-man-who-tried-to-redeem-the-world-with-logic)

